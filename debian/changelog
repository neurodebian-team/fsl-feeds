fsl-feeds (5.0.4-1) data; urgency=low

  * New upstream release.
  * Sort md5sum list for clarity.
  * Adjust package dependencies to cope with FSL package name change.

 -- Michael Hanke <mih@debian.org>  Fri, 10 May 2013 16:20:56 +0200

fsl-feeds (5.0.0-1) data; urgency=low

  * New upstream release. Only minor changes to the RUN script. All data files
    remain identical.
  * No longer force sourcing of fsl.sh in fsl-selftest. If FSLDIR is already
    set, no configuration file is sourced. This should allow for testing of
    multiple installed FSL versions -- not just the default.

 -- Michael Hanke <mih@debian.org>  Thu, 06 Sep 2012 09:31:32 +0200

fsl-feeds (4.1.9-1) data; urgency=low

  * Switch to source package format 3.0 (quilt).
  * Add patch to make the test suite compatible with cluster job submission
    mode.
  * Move fsl-first-data from Depends to Recommends. Most of the test suite
    runs without it and the download size is significant (1 GB).

 -- Michael Hanke <mih@debian.org>  Tue, 29 Nov 2011 09:10:50 +0100

fsl-feeds (4.1.8-1) data; urgency=low

  * New upstream release.
  * Bumped Standards-Version to 3.9.2 -- no changes necessary.

 -- Michael Hanke <mih@debian.org>  Thu, 07 Jul 2011 11:57:20 -0400

fsl-feeds (4.1.4-1) unstable; urgency=low

  * New upstream release.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 03 Jun 2009 20:19:53 +0200

fsl-feeds (4.1.3-1) unstable; urgency=low

  * New upstream release.
  * Bumped Standards-Version to 3.8.1 -- no changes necessary.
  * Update debhelper compat level to 5.

 -- Michael Hanke <michael.hanke@gmail.com>  Tue, 28 Apr 2009 14:54:17 +0200

fsl-feeds (4.1.0-1) unstable; urgency=low

  * New upstream release.
  * Bumped Standards-Version to 3.8.0 -- no changes necessary.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 14 Aug 2008 14:40:05 +0200

fsl-feeds (4.0.1-2) unstable; urgency=low

  * Added bc to the package dependencies. 

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 27 Sep 2007 11:01:20 +0200

fsl-feeds (4.0.1-1) unstable; urgency=low

  * New upstream release.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 13 Sep 2007 20:34:13 +0200

fsl-feeds (4.0.0-1) unstable; urgency=low

  * New upstream snapshot.
  * Added a proper license statement to 'fsl-selftest'.
  * Converted the package to CDBS.
  * To prevent wasting archive space by having the datasets in the source and
    the binary packages, the source package is now virtually empty. Please see
    README.Debian-source for more information.
  * Abandoned dpatch build-dep.
  * Add dependency to the new FSL data packages.
  * Updated manpage to include new 'first' test.
  * Updated debian/copyright.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 16 Aug 2007 17:25:52 +0200

fsl-feeds (3.3.0-4) unstable; urgency=low

  * Change dependency from fsl-sbrain to fsl-data.
  * Bumped Standards-Version to 3.7.2. 

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 15 Nov 2006 10:29:58 +0100

fsl-feeds (3.3.0-3) unstable; urgency=low

  * Added dependency to the fsl-sbrain package. 

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 13 Sep 2006 15:38:30 +0200

fsl-feeds (3.3.0-2) unstable; urgency=low

  * Started using dpatch.
  * Fixed path to in call to tclsh. 

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 12 Apr 2006 08:26:47 +0200

fsl-feeds (3.3.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Hanke <michael.hanke@gmail.com>  Sat,  8 Apr 2006 08:31:41 +0200

fsl-feeds (3.2-3) unstable; urgency=low

  * Improved fsl-selftest script. Added possibility to set an option to clean 
    the test results after the test run. Updated manpage.

 -- Michael Hanke <michael.hanke@gmail.com>  Fri, 24 Feb 2006 10:07:53 +0100

fsl-feeds (3.2-2) unstable; urgency=low

  * Bugfix: Source fsl.sh from original position in /usr/share/fsl/etc/fslconf

 -- Michael Hanke <michael.hanke@gmail.com>  Tue,  1 Nov 2005 12:33:02 +0100

fsl-feeds (3.2-1) unstable; urgency=low

  * Initial Release.

 -- Michael Hanke <michael.hanke@gmail.com>  Fri, 30 Sep 2005 09:03:19 +0200

